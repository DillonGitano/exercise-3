﻿using Desktop_Exercise_3.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;

namespace Desktop_Exercise_3
{
  class Program
  {
    private static void WriteCustomerDetails(StreamWriter writer, List<Customer> customerList)
    {
      foreach (var customer in customerList)
      {
        writer.WriteLine(customer.Person.FirstName);
      }
    }

    static void Main(string[] args)
    {
      var myPeople = new List<Person>();
      var myCustomers = new List<Customer>();

      var customerAccountNumbers = new List<string>();
      var customerDetails = new List<Customer>();
      

      //keep this one and move all queries into it
      using (var db = new AdventureWorks2014Entities())
      {        
        //Query 2
        customerDetails = db.Customers
          .Include(x => x.Store)
          .Include(x => x.Person)
          .Include(x => x.SalesOrderHeaders)
          .ToList();

        //var totalSales = (from x in db.SalesOrderHeaders
        //                  select x.TotalDue)
        //                  .Sum();

        //var bigSpenda = (from sum in db.SalesOrderHeaders
        //                 join name in db.Customers on sum.CustomerID equals name.CustomerID
        //                 )
                         
        //Query 4 ALMOST
         var bigSpenda = db.Customers
          .Include(x => x.SalesOrderHeaders.Where(y => y.TotalDue)
          .Sum(); 

      }

      //Query 4 Output
      var q4Path = @"../../q4Path.txt";

      using (var swq4 = new StreamWriter(q4Path))
      {
        foreach (var p in customerDetails)
        {
          swq4.WriteLine("{0}, {1}, {2}, {3}, {4}",
            p.CustomerID,
            (p.Person != null) ? p.Person.LastName : "N/A",
            (p.Person != null) ? p.Person.FirstName : "",
            p.AccountNumber,
            (p.Store != null) ? p.Store.Name : "N/A"
            );

          foreach (var s in p.SalesOrderHeaders)
          {
            swq4.WriteLine("Subtotal: {0:C}, Tax: {1:C}, Freight: {2:P}, Total: {3:C}",
            s.SubTotal,
            s.TaxAmt,
            (s.Freight / s.SubTotal),
            s.TotalDue);            
          }
          swq4.WriteLine("");
        }
        
        swq4.WriteLine("There are {0} customers", customerDetails.Count);
      }
    }
  }
}
